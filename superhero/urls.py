
from .views import SuperheroView
from rest_framework.routers import DefaultRouter
from django.urls import path, include
from .views import MemberList, test_view
from django.conf.urls import include, url

router = DefaultRouter()
router.register(r'superhero', SuperheroView, basename='superhero')

# router.register(r'members', MemberViewset, basename='members')
urlpatterns = [
    url(r'^members/', MemberList.as_view()),
    url(r'^homes/', test_view)
]


