from rest_framework import serializers
from .models import Superhero


class SuperheroSerializer(serializers.ModelSerializer):
    members = serializers.JSONField()

    class Meta:
        model = Superhero
        fields = '__all__'
    def create(self, validated_data):
        squad_name = validated_data['squad_name']
        hometown = validated_data['hometown']
        active = validated_data['active']
        formed = validated_data['formed']
        validated_data['members'] = {"squad_name": squad_name, "hometown": hometown, "active": str(active), "formed": str(formed)}
        all_data = Superhero.objects.create(**validated_data)
        return all_data
    def update(self, instance, validated_data):
        members_data = validated_data.pop('members')
        # print("validated_data", validated_data)
        instance.squad_name = validated_data.get('squad_name', instance.squad_name)
        instance.hometown = validated_data.get('hometown', instance.hometown)
        instance.active = validated_data.get('active', instance.active)
        instance.formed = validated_data.get('formed', instance.formed)
        instance.members['squad_name'] = validated_data.get('squad_name', instance.members['squad_name'])
        instance.members['hometown'] = validated_data.get('hometown', instance.members['hometown'])
        instance.members['active'] = str(validated_data.get('active', instance.members['active']))
        instance.members['formed'] = str(validated_data.get('formed', instance.members['formed']))
        instance.save()
        return instance      
class MemberSerializer(serializers.ModelSerializer):
    members = serializers.JSONField()
    class Meta:
        model = Superhero
        fields = ('members',)





