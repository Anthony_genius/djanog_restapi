"""rest101 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf.urls import include, url
from superhero import views as superhero_views
from rest_framework.routers import DefaultRouter
from django.urls import path, include

router = DefaultRouter()
router.register(r'superhero', superhero_views.SuperheroView, basename='superhero')
router.register(r'members', superhero_views.MemberView, basename='members')

# router.register(r'members/?activen=True', superhero_views.MemberActiveView, basename='members/?activen=True')
urlpatterns = [
    path('api/', include(router.urls)),
    url(r'^search/', include('superhero.urls')),
]




